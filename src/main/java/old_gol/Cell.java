package old_gol;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

public class Cell extends Rectangle {

    private static final Color DEAD_COLOR = Color.WHITE;
    private static final Color ALIVE_COLOR = Color.BLACK;

    private ArrayList<ArrayList<Cell>> map;
    private int old_status = 0;
    private int new_status = 0;
    public int POSX = 0;
    public int POSY = 0;


    public Cell(ArrayList<ArrayList<Cell>> gridmap, int posx, int posy, int x, int y, int width, int height) {
        super(x, y, width, height);

        this.map = gridmap;
        this.POSX = posx;
        this.POSY = posy;

        turnDead();
        this.setStroke(Color.GHOSTWHITE);
        this.setStrokeWidth(1);
        this.setOnMouseDragEntered((EventHandler<MouseEvent>) event -> manualPaint(event));
        this.setOnMouseClicked(event -> manualPaint(event));
    }

    private int getNeighbor(int x, int y) {
        if(POSX + x < 0 ||POSY +  y < 0 || POSX + x >= this.map.size() || POSY + y >= this.map.get(0).size()) return 0;
        return this.map.get(POSX + x).get(POSY + y).getStatus();
    }

    public void update() {
        int active_neighbors = countLivingNeighbors();
        if(active_neighbors == 3) turnAlive();
        else if(this.getStatus() == 1 && (active_neighbors < 2 || active_neighbors > 3)) turnDead();
    }

    public int countLivingNeighbors() {
        return  getNeighbor(-1, -1) +
                getNeighbor(0, -1) +
                getNeighbor(+1, -1) +
                getNeighbor(-1, 0) +

                getNeighbor(+1, 0) +
                getNeighbor(-1, +1) +
                getNeighbor(0, +1) +
                getNeighbor(+1, +1);
    }

    private int getStatus() {
        return this.old_status;
    }

    private void manualPaint(MouseEvent event) {
        if(event.getButton() == MouseButton.PRIMARY) {
            turnAlive();
            repaint();
        }
        else if(event.getButton() == MouseButton.SECONDARY) {
            turnDead();
            repaint();
        }
    }

    public void turnAlive() {
        this.new_status = 1;
    }

    public void turnDead() {
        this.new_status = 0;
    }

    public void repaint() {
        this.setFill( new_status == 0 ? DEAD_COLOR : ALIVE_COLOR );
        this.old_status = this.new_status;
    }
}
