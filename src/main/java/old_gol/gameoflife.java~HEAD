package old_gol;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.ArrayList;

public class gameoflife extends Application {

    public static final int CELLSIZE = 30; // Size of a cell in pixels
    public static final int PERIMETER = 30; // Amount of cells per row/column. These are always equal, thus forming a square.
    public static final int TIMER_SPEED = 300; // In milliseconds

    public static final String TITLE = "Game of Life! ";
    public static final String TIMELINE_LABEL = "Timeline: ";
    public static final String RUNNING_STRING = " Running!";
    public static final String STOPPED_STRING = " Stopped!";

    public int TIMELINE = 0;
    public String RUNNING_STATUS = RUNNING_STRING;

    private final ArrayList<ArrayList<Cell>> gridmap = new ArrayList<>();
    private static final int WORLDSIZE = CELLSIZE * PERIMETER;
    private static final int loopsize = WORLDSIZE / CELLSIZE;

    private boolean running = false;
    private AnimationTimer time;
    private Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        Group root = new Group();
        Canvas canvas = new Canvas(WORLDSIZE - 11, WORLDSIZE-11);
        Scene scene = new Scene(root);
        root.getChildren().add(canvas);

        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        updateTitle();
        primaryStage.show();

        drawGrid(root);

        time = new AnimationTimer() {
            long old_time = -1;
            @Override
            public void handle(long now) {
                if(old_time == -1) old_time = now / 1000000;
                long new_time = now / 1000000;
                if( (new_time - old_time) > TIMER_SPEED ) {
                    update();
                    TIMELINE++;
                    updateTitle();

                    old_time = now / 1000000;
                }
            }
        };

        scene.addEventFilter(KeyEvent.KEY_PRESSED, ke -> {
            if (ke.getCode() == KeyCode.SPACE) {
                if(running) {
                    running = false;
                    time.stop();
                    RUNNING_STATUS = STOPPED_STRING;
                    updateTitle();
                }
                else {
                    running = true;
                    time.start();
                    RUNNING_STATUS = RUNNING_STRING;
                    updateTitle();
                }
                ke.consume(); // <-- stops passing the event to next node
            }
        });

        scene.addEventFilter(MouseEvent.DRAG_DETECTED , mouseEvent -> scene.startFullDrag());
    }

    private void drawGrid(Group root) {
        for(int x = 0; x < loopsize; x ++) {
            ArrayList<Cell> row = new ArrayList<>();
            for (int y = 0; y < loopsize; y ++) {

                Cell rect = new Cell(gridmap, x, y,x * CELLSIZE, y * CELLSIZE, CELLSIZE - 1,CELLSIZE - 1);


                row.add(rect);
                root.getChildren().add(rect);
            }
            gridmap.add(row);
        }
        update();
    }

    private void update() {
        for(int x = 0; x < loopsize; x ++)
            for (int y = 0; y < loopsize; y ++)
                gridmap.get(x).get(y).update();

        for(int x = 0; x < loopsize; x ++)
            for (int y = 0; y < loopsize; y++)
                gridmap.get(x).get(y).repaint();
    }

    private void updateTitle() {
        primaryStage.setTitle(TITLE + TIMELINE_LABEL + TIMELINE + RUNNING_STATUS);
    }

}