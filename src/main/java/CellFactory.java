import javafx.scene.Group;
import java.util.HashMap;
import java.util.Map;

public class CellFactory {

    private int[][] map;
    private final int CELLSIZE;
    private Map<String, Celly> Cells = new HashMap<>();

    private CellFactory() {CELLSIZE=0;} //To Disable Empty Instances
    public CellFactory(int CellSize, Group root, int sizex, int sizey) {
        this.CELLSIZE = CellSize;
        this.map = new int[sizex][sizey];
    }

    public void update() {
        int[][] tempmap = new int[this.map.length][this.map[0].length];

        for (int lx = 0; lx < tempmap.length; lx++) {
            for (int ly = 0; ly < tempmap.length; ly++) {
                tempmap[lx][ly] = nextStatus(lx, ly);
            }
        }
        for (int lx = 0; lx < this.map.length; lx++) {
            for (int ly = 0; ly < this.map[0].length; ly++) {
                this.map[lx][ly] = tempmap[lx][ly];
                if (this.map[lx][ly] == 1) getCell(lx, ly).turnAlive();
                else getCell(lx, ly).turnDead();
            }
        }
    }

    public void insertCells(Group root, int loop) {
        for(int lx = 0; lx < loop; lx++) {
            for(int ly = 0; ly < loop; ly++) {
                map[lx][ly] = 0;
                Celly cell = new Celly(lx,ly,CELLSIZE);
                Cells.put(lx+":"+ly, cell);
                root.getChildren().add(cell);
            }
        }
    }

    private Cellx getCell(int posx, int posy) {
        return Cells.get(posx+":"+posy);
    }

    private int getNeighbor(int x, int y, int reposx, int reposy) {
        if( (x+reposx) < 0 || (y+reposy) < 0 || (x+reposx) >= this.map.length || (y+reposy) >= this.map[0].length) return 0;
        return this.map[x+reposx][y+reposy];
    }

    private int countLivingNeighbors(int x, int y) {
        return  getNeighbor(x,y,-1, -1) +
                getNeighbor(x,y,0, -1) +
                getNeighbor(x,y,+1, -1) +
                getNeighbor(x,y,-1, 0) +

                getNeighbor(x,y,+1, 0) +
                getNeighbor(x,y,-1, +1) +
                getNeighbor(x,y,0, +1) +
                getNeighbor(x,y,+1, +1);
    }

    private int nextStatus(int x, int y) {
        int active_neighbors = countLivingNeighbors(x,y);
        if(active_neighbors == 3) return 1;
        else if(this.map[x][y] == 1 && (active_neighbors < 2 || active_neighbors > 3)) return 0;
        else return this.map[x][y];
    }

}
