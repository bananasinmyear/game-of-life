import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class Celly extends Rectangle implements Cellx{

    private final int posx;
    private final int posy;

    private static final Color DEAD_COLOR = Color.GRAY;
    private static final Color ALIVE_COLOR = Color.BLACK;

    public Celly(int posx, int posy, int CELLSIZE) {
        super(CELLSIZE * posx, CELLSIZE * posy, CELLSIZE, CELLSIZE);
        this.posx = posx;
        this.posy = posy;

        this.setStroke(Color.GHOSTWHITE);
        this.setStrokeWidth(1);
        this.setOnMouseDragEntered((EventHandler<MouseEvent>) event -> manualPaint(event));
        this.setOnMouseClicked(event -> manualPaint(event));
    }

    private void manualPaint(MouseEvent event) {
        if(event.getButton() == MouseButton.PRIMARY) {
            turnAlive();
        }
        else if(event.getButton() == MouseButton.SECONDARY) {
            turnDead();
        }
    }

    @Override
    public void turnAlive() {
        this.setFill(ALIVE_COLOR);
    }

    @Override
    public void turnDead() {
        this.setFill(DEAD_COLOR);
    }
}
